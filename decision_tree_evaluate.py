from sklearn import tree
from sklearn.datasets import load_iris
import numpy as np
import random


def decision_tree_classifier(trainx, trainy, testx, testy, c):
    clf = tree.DecisionTreeClassifier()
    clf = clf.fit(trainx, trainy)
    predicts = clf.predict(testx)
    y = testy
    c = 0
    for i in range(len(predicts)):
        if predicts[i] == y[i]:
            c += 1
    return (c / len(testy))


iris = load_iris()
data = iris.data
target = iris.target
l_index = list(range(len(data)))
random.shuffle(l_index)
data = data[l_index]
target = target[l_index]
print(data.shape, target.shape)
pieces_len = len(data) / 3
precision = 0
for j in range(100):
    for i in range(3):
        si = int(i * pieces_len)
        ei = int((i + 1) * pieces_len)
        testx = data[si:ei]
        testy = target[si:ei]
        trainx = np.concatenate((data[:si], data[ei:]), axis=0)
        trainy = np.concatenate((target[:si], target[ei:]), axis=0)
        precision += decision_tree_classifier(trainx=trainx, trainy=trainy, testx=testx, testy=testy, c=i)
print(precision / 300)
